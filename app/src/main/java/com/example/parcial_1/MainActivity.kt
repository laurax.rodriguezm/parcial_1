package com.example.parcial_1

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val boton = findViewById<Button>(R.id.continuar)
        val editnombre = findViewById<EditText>(R.id.nombre)
        val msnerror= findViewById<TextView>(R.id.msnerror)
        val editpssw= findViewById<EditText>(R.id.password)


        boton.setOnClickListener {
            val nombre = editnombre.text.toString()
            val password: String = editpssw.text.toString()


            if (nombre =="") {
                msnerror.text="El campo de nombre es obligatorio"

            }else{  if (nombre.equals("yamiley.lagaresa@ecci.edu.co" ) || nombre.equals("yorladis.grisalesh@ecci.edu.co" ) ||nombre.equals("laurax.rodriguezm@ecci.edu.co") ){

                if (password ==""){
                        msnerror.text="El campo de contraseña es obligatorio"
                    }else{
                        val intent= Intent(this, WelcomeActivity:: class.java )
                    startActivity(intent)

                    }

                } else{
                msnerror.text="Usuario incorrecto"
            }
                }
            }


        }
    }


